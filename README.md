# ansible

ansible — запуск отдельных команд и модулей;

ansible-console — запуск команд и модулей в интерактивном режиме;

ansible-doc — документация по модулям;

ansible-galaxy — инструмент для управления ролями;

ansible-playbook — запуск плейбуков;

ansible-pull — позволяет забирать конфигурации и плейбуки из GIT и затем выполнять;

ansible-vault — инструмент для шифрования плейбуков и паролей.

Порядок выполнения конфигурационных файлов:
$ANSIBLE_CONFIG
./ansible.cfg
~/.ansible.cfg
/etc/ansible/ansible.cfg
Важно!
Конфигурации НЕ сливаются, Ansible начнет использовать первый найденный источник!

Особенные модули:
command — принимает команду с аргументами и выполняет ее на управляемых серверах. Эта команда будет выполнена не в shell. То есть пайпы (<,>,|) и переменные окружения ($HOME) работать не будут. Модуль считается наиболее безопасным способом для выполнения простых команд.
shell — то же самое, что и предыдущий модуль, однако команда с аргументами выполняется уже в оболочке (/bin/sh, /bin/bash), и все, что работает в shell, будет работать с этим модулем. Тут рекомендуется вызывать переменные с помощью фильтра: {{myvar|quote}}, чтобы обезопасить себя от возможного мусора в переменной (типа ";" и т.д.).
raw — особенность этого модуля в том, что для того, чтобы выполнить команды с помощью него, на удаленной машине вообще может быть не установлен Python. С помощью этого модуля запускается «грязная» SSH-команда, без загрузки какой-либо Python-инфраструктуры инструмента Ansible.
script — так же, как и raw — не требует установленного Python на управляемой машине. Модуль берет скрипт в локальной папке, отправляет на удаленный сервер и выполняет его в shell со всеми аргументами, с которыми мы захотели его передать.

[Настройка виртуальной машины и установка ansible](#настройка)
[Инвентарный файл](#инвентарныйфайл)
[Создание роли](#роли)
[Основные действия ansible](#действия)
[Запуск ansible](#запуск)
[Обработка ошибок в плейбуке](#ошибки)

<a name="настройка"></a>

## Настройка виртуальной машины и установка ansible

- захожу на Виртуальную Машину (далее ВМ)

    ` $ ssh login@ip `

    ` $ ввожу свой пароль `

- подложить свой публичный ключ на ВМ, чтобы не заходить по паролю

    + SSH-сервер по умолчанию не установлен в Ubuntu, устанавливаем на машину

    ` $ sudo apt install openssh-server `

    + генерим свой ssh ключ

    ` $ ssh-keygen `

    ` $ cat ~/.ssh/id_rsa.pub `

    + вставляю ключ свой публичный в файл (через вим например или с помощью echo >> )

    ` $ touch /home/liga/.ssh/authorized_keys `

    ` $ chmod 600 ~/.ssh/authorized_keys `

    ` $ sudo systemctl restart ssh `

- поставить ансибл на локальную ВМку
    + скачаю ансбл

    ` $ sudo apt install ansible `

    ` $ ansible --version `

    + настраиваю подключение к ВМ

    ` $ sudo vim /etc/ansible/hosts `

        [servers]
        server1 ansible_host=172.127.127.127

    + проверяю настройку подключения ВМ

    ` $ ansible-inventory --graph `

    ` $ ansible-inventory --list -y `

    ` $ ansible all -m ping -u user `

<a name="инвентарныйфайл"></a>

## Инвентарный файл

Список хостов хранится в инвентарных файлах которые имеют разрешение *.ini или *.yml. Ниже будет описан пример *.ini файла.

    # комментарий обозначается через решетку

    [<group_name>]
    <server_name> ansible_host=127.0.0.1

    [<group_name>:vars]
    <var>=<value>

Переменные которые могут потребоваться:

    # логин для подключения
    ansible_user='login'

    # пароль для подключения
    ansible_password='password'

Проверить корректность записи файла можно

` $ ansible-inventory -i <inventory_file> --list `

Проблема с которой я столкнулась:

    gitlab-runner-2 | FAILED! => {
        "msg": "Using a SSH password instead of a key is not possible because Host Key checking is enabled and sshpass does not support this.  Please add this host's fingerprint to your known_hosts file to manage this host."
    }

Решение:

` $ sudo vim /etc/ansible/ansible.cfg `

        [defaults]
        host_key_checking = false

<a name="роли"></a>

## Создание роли

!! любую последовательность действий для одной кокнретной задачи лучше записывать через роль (чтобы в дальнейшем было легче ее применять)

- создаем роль в ансбл

` $ cd /etc/ansible `

` $ sudo ansible-galaxy init role_name `

` $ cd role_name/tasks/ `

` $ sudo vim main.yml `

<a name="действия"></a>

## Основные действия ansible

!! Тк формат файла yml то важно количество пробелов в начале строк

- создать пользователя

    \- name: create user
    user:
        name: login
        password: "{{ 'password' | password_hash('sha512') }}"

- загрузить что-либо (например nginx)

    \- name: download nginx
    apt:
        name: nginx
        state: latest

- настроить разворачивание шаблонных файлов (например конфиги nginx)

    + создаем шаблонные файла. при необходимости добавить переменные указываем их в двойных фигурных скобках
например, часть шаблона конфига, где в 28 строке присутствует переменная:

    ```
        26            userid         on;
        27            userid_name    uid;
        28            userid_domain  {{ var_userid_domian }};
        29            userid_path    /;
        30            userid_expires 1d;
    ```

    + добавляем в ./name_role/vars/main.yml значение переменной

    ```
        var_userid_domian: "sed.rtech.ru"
    ```

    + добавляем в ./role_name/tasks/main.yml

    ```
        - name: add nginx.conf
            template:
            src: nginx.conf.template
            dest: /etc/nginx/nginx.conf
    ```

<a name="запуск"></a>

## Запуск ansible

- запускаем плейбук чтобы проверить что работает

` $ cd /etc/ansible `

` $ sudo touch main.yml `

` $ sudo vim main.yml `

```
    ---
    - hosts: all
        remote_user: user
        vars:
        ansible_sudo_pass: password
        become: yes
        become_method: sudo
        roles:
        - role_name
```

` $ ansible-playbook main.yml -u user -i <ip> -K `

// (где -К - ввод пароля после подключения ссш)

// (можно добавить ключ -k чтобы запрашивался пароль к подключению по ссш)

- запускаем локально

` $ ansible-playbook /etc/ansible/main.yml -i "127.0.0.1," -c local `


<a name="ошибки"></a>

## Обработка ошибок в плейбуке

(прописывается в таске)

    игнорирование неудачных команд = ignore_errors: yes

    игнорирование недоступных ошибок хоста = ignore_unreachable: yes

(прописывается в начале плейбука)

    установка максимального процента отказа = max_fail_percentage: 30

    прерывание первой ошибки = any_errors_fatal: true
